
public class Product {
	public final int type;
	public final int weight;
	
	public Product(int type, int weight) {

		this.type = type;
		this.weight = weight;
	}

}
