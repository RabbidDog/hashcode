import java.util.*;
public class Order {
	
	public final Location loc;
	public final int id; 
	public final Hashtable<Integer, ProductBag> orderDetail; /*productCOunt in ProductBag will represent remaning order amount*/
	public boolean completed;
	
	public Order(int id, Location loc, Hashtable<Integer, ProductBag> orderDetails) {
		this.id = id;
		this.loc = loc;
		this.orderDetail = orderDetails;
		
	}

}
