import java.util.*;
public class Warehouse {

	public final int id;
	public final int location;
	public Hashtable<Integer, Product> products;
	public Warehouse(int id, int location) {

		this.id = id;
		this.location = location;
		this.products = new Hashtable<Integer, Product>();
	}

}
