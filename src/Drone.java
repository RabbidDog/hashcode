import java.lang.*;
import java.util.Hashtable;
public class Drone {

	public final int id;
	public Location loc;
	public Hashtable<Integer, ProductBag> droneInventory;
	public final int maxCapacity;
	private int _currentWeight;
	
	public Drone(int id, int capacity) {
		this.id = id;
		this.maxCapacity = capacity;
		this.droneInventory = new Hashtable<Integer, ProductBag>();
	}
	
	public int getRemainingCapacity()
	{
		return (maxCapacity - _currentWeight);
	}
	
	public void load(Product product, int count) throws Exception
	{
		int totalWeightAddition = product.weight * count;
		if(totalWeightAddition > (maxCapacity - _currentWeight))
		{
			System.out.println("maximum load exceeded for drone" + id);
			throw new Exception();
		}
		
		_currentWeight += totalWeightAddition;
		ProductBag bag = droneInventory.getOrDefault(product.type, new ProductBag(product.type));
		bag.productCount += totalWeightAddition;
		droneInventory.put(product.type, bag);
		
	}
	
	/*assuming required number of productype already present*/
	public void unload(Product product, int count)
	{
		int totalWeightLoss = product.weight * count;
		
		ProductBag bag = droneInventory.getOrDefault(product.type, new ProductBag(product.type));
		bag.productCount -= totalWeightLoss;
		droneInventory.put(product.type, bag);
	}

}
