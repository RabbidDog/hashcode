
public class ProductBag {

	public final int productType;
	public int productCount;
	public ProductBag(int productType) {
		this.productType = productType;
	}
	
	public ProductBag(int productType, int productCount) {
		this.productType = productType;
		this.productCount = productCount;
	}
	
}
