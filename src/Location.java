import java.math.*;
public class Location {

	public final int row;
	public final int col;
	public Location(int row, int col) {
		this.row = row;
		this.col = col;
	}
	
	public long getDistance(Location toLoc)
	{
		long dist =(long) Math.ceil( Math.sqrt(((row - toLoc.row)*(row - toLoc.row)) + (col - toLoc.col) * (col - toLoc.col)));
		return dist;
	}

}
